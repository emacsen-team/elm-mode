(define-package "elm-mode" "0.20.3"
  "Major Emacs mode for editing Elm source code"
  '((f "0.17") (let-alist "1.0.4") (s "1.7.0") (emacs "24")))
